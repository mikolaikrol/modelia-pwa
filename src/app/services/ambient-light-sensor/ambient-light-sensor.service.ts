import { Injectable } from '@angular/core';
import {Observable, ReplaySubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AmbientLightSensorService {
  private illuminance: ReplaySubject <number> = new ReplaySubject <number>(1);
  illuminance$: Observable<number> = this.illuminance.asObservable();

  constructor(private window: Window) {
    try {
      if ("AmbientLightSensor" in window) {
        this.startReading();
      } else {
        this.illuminance.error("Cet appareil ne supporte pas cette fonctionnalité.");
      }
    } catch (error) {
      // Handle construction errors.
      if (error.name === "SecurityError") {
        this.illuminance.error("Capteur bloqué");
      } else if (error.name === "ReferenceError") {
        this.illuminance.error("Capteur non supporté");
      } else {
        this.illuminance.error(`${error.name}: ${error.message}`);
      }
    }
  }

  private startReading() {
    // @ts-ignore
    const sensor = new AmbientLightSensor();
    sensor.onreading = () => this.illuminance.next(sensor.illuminance);
    sensor.onerror = async event => {
      // Handle runtime errors.
      if (event.error.name === "NotAllowedError") {
        // Branch to code for requesting permission.
        const result = await navigator.permissions.query({
          name: "ambient-light-sensor"
        });
        if (result.state === "denied") {
          this.illuminance.error("Permission refusée.");
          return;
        }
        this.startReading();
      } else if (event.error.name === "NotReadableError") {
        this.illuminance.error("Connexion au capteur impossible");
      }
    };
    sensor.start();
  }
}
