import {Injectable} from '@angular/core';
import User from "../../models/User";
import {LocalStorageService} from "../local-storage/local-storage.service";
import {Observable, Subject} from "rxjs";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public API_URL = 'https://modelia-api.herokuapp.com';

  private _user: User | null;
  private _userSubject = new Subject<User | null>();
  private _isGuest: boolean | null;
  private _isGuestSubject = new Subject<boolean | null>();

  constructor(
    protected localStorageService: LocalStorageService,
    protected router: Router,
    private http: HttpClient,
  ) {
    this._user = this.localStorageService.get(LocalStorageService.KEYS.user);
    this._isGuest = this.localStorageService.get(LocalStorageService.KEYS.isGuest);
  }

  getUser() {
    return this._user;
  }

  setUser(user: User | null) {
    this._user = user;
    this._userSubject.next(this._user);
    this.localStorageService.set(LocalStorageService.KEYS.user, this._user);
  }

  isGuest() {
    return this._isGuest;
  }

  setIsGuest(isGuest: boolean | null) {
    this._isGuest = isGuest;
    this._isGuestSubject.next(this._isGuest);
    this.localStorageService.set(LocalStorageService.KEYS.isGuest, this._isGuest);
  }

  watch(key: string): Observable<any> {
    switch (key) {
      case 'user':
        return this._userSubject.asObservable();
      case 'isGuest':
        return this._isGuestSubject.asObservable();
    }

    return new Observable<any>();
  }

  loginAsGuest(): void {
    this.setUser(User.GUEST);
    this.setIsGuest(true);
    this.router.navigate(['/accueil']);
  }

  login(data) {
    return this.http.post(this.API_URL + '/authentication', {strategy: 'local', ...data});
  }

  fetch() {
    const token = this.localStorageService.get('token');
    return this.http.get(this.API_URL + `/users/${this._user ? this._user["_id"] : ''}`, {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    });
  }

  logout(): void {
    this.setUser(null);
    this.setIsGuest(null);
  }
}
