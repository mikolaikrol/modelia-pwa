import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {StyleManagerService} from "../style-manager/style-manager.service";
import {LocalStorageService} from "../local-storage/local-storage.service";


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  public static readonly THEMES = {
    LIGHT: 'deeppurple-amber',
    DARK: 'pink-bluegrey',
  }

  private currentTheme: string;

  constructor(
    private http: HttpClient,
    private styleManager: StyleManagerService,
    private localStorageService: LocalStorageService,
  ) {}

  getCurrentTheme(): string {
    return this.currentTheme;
  }

  setTheme(themeToSet: string) {
    this.currentTheme = themeToSet;
    this.localStorageService.set('theme', themeToSet);
    this.styleManager.setStyle(
      "theme",
      `assets/themes/${themeToSet}.css`
    );
  }
}