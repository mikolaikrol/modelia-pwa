import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {LocalStorageService} from "../local-storage/local-storage.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  BASE_URL = "https://modelia-api.herokuapp.com/activities/";

  constructor(
    private http: HttpClient,
    private storage: LocalStorageService,
  ) {
  }

  public create(activity: any): Observable<any> {
    const token = this.storage.get('token');
    return this.http.post(this.BASE_URL, activity, {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json"
      }
    });
  }
}
