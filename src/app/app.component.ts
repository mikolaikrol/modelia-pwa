import {Component, HostListener, Inject, ViewChild} from '@angular/core';
import {NetworkService} from "./services/network/network.service";
import {MonitorComponent} from "./components/monitor/monitor.component";
import {LocalStorageService} from "./services/local-storage/local-storage.service";
import {UserService} from "./services/user/user.service";
import User from "./models/User";
import {Router} from "@angular/router";
import {MatSidenav} from "@angular/material/sidenav";
import {ThemeService} from "./services/theme/theme.service";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {AmbientLightSensorService} from "./services/ambient-light-sensor/ambient-light-sensor.service";
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends MonitorComponent {

  @ViewChild('sidenav') private sidenav: MatSidenav;

  title = 'Modelia';
  user: User | null;
  isGuest: boolean | null;
  loading = true;
  isDark: boolean;

  constructor(
    protected networkService: NetworkService,
    protected userService: UserService,
    protected themeService: ThemeService,
    protected localStorageService: LocalStorageService,
    protected alsService: AmbientLightSensorService,
    protected router: Router,
    @Inject(DOCUMENT) private readonly doc: Document
  ) {
    super(networkService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.user = this.userService.getUser();
    this.isGuest = this.userService.isGuest();
    this.subscribe();
    this.isDark = this.localStorageService.get('theme') === ThemeService.THEMES.DARK;
    this.themeService.setTheme(this.isDark ? ThemeService.THEMES.DARK : ThemeService.THEMES.LIGHT);
    this.loading = false;
  }

  @HostListener('document:visibilitychange')
  handleVisibilityChange(): void {
    if (this.doc.hidden) {
      this.unsubscribe();
    } else {
      this.subscribe();
    }
  }

  logout(): void {
    this.userService.logout();
    this.sidenav.toggle();
    this.router.navigate(['/login']);
  }

  clearSession() {
    this.localStorageService.remove(LocalStorageService.KEYS.activities);
    this.logout();
  }

  onToggleChanged(event: MatSlideToggleChange): void {
    this.themeService.setTheme(event.checked ? ThemeService.THEMES.DARK : ThemeService.THEMES.LIGHT);
  }

  subscribe() {
    this.subscriptions$.push(
      this.alsService.illuminance$.subscribe(
        illu => {
          this.themeService.setTheme(illu <= 10 ? ThemeService.THEMES.DARK : ThemeService.THEMES.LIGHT);
          this.isDark = illu <= 10;
        }
      ),
      this.networkService.monitor().subscribe(
        isOnline => this.online = isOnline
      ),
      this.userService.watch(LocalStorageService.KEYS.user).subscribe(user => this.user = user),
      this.userService.watch(LocalStorageService.KEYS.isGuest).subscribe(isGuest => this.isGuest = isGuest),
    )
  }

  unsubscribe() {
    this.subscriptions$.forEach(sub => sub.unsubscribe());
    this.subscriptions$ = [];
  }
}
