import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./guards/auth.guard";
import {PendingChangesGuard} from "./guards/pending-changes.guard";

const routes: Routes = [
  {path: '', loadChildren: () => import('./controllers/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard]},
  {path: 'accueil', loadChildren: () => import('./controllers/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard]},
  {path: 'login', loadChildren: () => import('./controllers/login/login.module').then(m => m.LoginModule), canActivate: [AuthGuard]},
  {path: 'activity', loadChildren: () => import('./controllers/activity/activity.module').then(m => m.ActivityModule), canActivate: [AuthGuard], canDeactivate: [PendingChangesGuard]},
  {path: 'exercises', loadChildren: () => import('./controllers/exercises/exercises.module').then(m => m.ExercisesModule), canActivate: [AuthGuard]},
  {path: 'profil', loadChildren: () => import('./controllers/profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuard]},
  {path: '**', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
