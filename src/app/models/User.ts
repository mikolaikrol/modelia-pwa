import {Activity} from "./Activity";

export default class User {
  id: string;
  mail: string | null;
  name: string;
  username: string;
  description: string;
  certified: boolean;
  picture: string | null;
  follows: User[];
  followers: User[];
  activities: Activity[];
  likedModels: Activity[];

  static readonly GUEST = new User('Invité', 'guest', 'guest');

  constructor(name: string, username: string, id: string, description?: string) {
    this.id = id;
    this.name = name;
    this.username = username;
    this.certified = false;
    this.description = description ?? 'Pas de description';
    this.mail = null;
    this.picture = null;
    this.follows = [];
    this.followers = [];
    this.activities = [];
    this.likedModels = [];
  }
}