import {Component, OnDestroy, OnInit} from '@angular/core';
import {NetworkService} from "../../services/network/network.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-monitor',
  template: '',
  styleUrls: []
})
export class MonitorComponent implements OnInit, OnDestroy {

  online: boolean;
  subscriptions$: Subscription[] = [];

  constructor(
    protected networkService: NetworkService,
  ) {
    this.online = navigator.onLine;
  }

  ngOnInit(): void {
    this.subscriptions$.push(
      this.networkService.monitor().subscribe(
        isOnline => this.online = isOnline
      )
    );
  }

  ngOnDestroy(): void {
    this.subscriptions$.forEach(sub => sub.unsubscribe());
  }

}
