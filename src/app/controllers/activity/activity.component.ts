import {Component} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {exerciseDefinitions} from "../../models/Exercise";
import {UserService} from "../../services/user/user.service";
import {LocalStorageService} from "../../services/local-storage/local-storage.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivityService} from "../../services/activity/activity.service";
import {MonitorComponent} from "../../components/monitor/monitor.component";
import {NetworkService} from "../../services/network/network.service";

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
})
export class ActivityComponent extends MonitorComponent {

  loading = false;
  modelForm: FormGroup;
  exercisesDefinition = exerciseDefinitions;

  constructor(
    protected networkService: NetworkService,
    private fb: FormBuilder,
    private userService: UserService,
    private storage: LocalStorageService,
    private snackbar: MatSnackBar,
    private activityService: ActivityService,
  ) {
    super(networkService);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.initForm();
  }

  initForm(): void {
    this.modelForm = this.fb.group({
      type: this.fb.control('model', [Validators.required]),
      title: this.fb.control('Nouvelle activité', [Validators.required]),
      description: this.fb.control(''),
      content: this.fb.array([this.fb.group({
        exercise: this.fb.control(exerciseDefinitions[0]),
        series: this.fb.array([this.newSerie()]),
      })]),
    })
  }

  get type(): string {
    return this.modelForm.value.type;
  }

  get exercises(): FormArray {
    return this.modelForm.get('content') as FormArray;
  }

  series(exIdx: number) {
    return this.exercises.at(exIdx).get('series') as FormArray;
  }

  newExercice() {
    return this.fb.group({
      exercise: this.fb.control(exerciseDefinitions[0]),
      series: this.fb.array([this.newSerie()]),
    });
  }

  addExercice() {
    this.exercises.push(this.newExercice());
  }

  newSerie() {
    return this.fb.group({
      reps: this.fb.control(0),
      kg: this.fb.control(0),
      validated: this.fb.control(false),
    })
  }

  addSerie(exIdx: number) {
    const series = this.exercises.at(exIdx).get('series') as FormArray;
    series.push(this.newSerie());
  }

  save() {
    this.loading = true;
    const activity = this.modelForm.value;
    activity.creationDate = new Date();
    const creator = this.userService.getUser();
    activity.creator = creator;
    if (creator) {
      activity.creatorId = creator.id ?? creator['_id'];
    }
    activity.categories = [];
    activity.content.forEach(e => {
      e.series = e.series.filter(s => s.reps > 0);
    });
    activity.content = activity.content.filter(e => e.series.length > 0);
    if (activity.content.length === 0) {
      alert('Votre activité est vide, veuillez la compléter');
      this.loading = false;
    } else if (creator?.id === 'guest' || !this.online) {
      const activities = this.storage.get(LocalStorageService.KEYS.activities) ?? [];
      activities.unshift(activity);
      this.storage.set(LocalStorageService.KEYS.activities, activities);
      this.snackbar.open('Activité enregistrée localement!', 'Fermer', {duration: 5000});
      this.initForm();
      this.loading = false;
    } else {
      this.activityService.create(activity).subscribe(() => {
          this.snackbar.open('Activité enregistrée !', 'Fermer', {duration: 5000});
          this.initForm();
          this.loading = false;
        },
        () => {
          this.snackbar.open('Erreur : l\'activité n\'a pas été enregistrée', 'Fermer', {duration: 5000});
          this.loading = false;
        })
    }
  }
}
