import { Component, OnInit, Input } from '@angular/core';
import { ExerciseDefinition } from 'src/app/models/Exercise';

@Component({
  selector: 'app-exercices-detail',
  templateUrl: './exercices-detail.component.html',
  styleUrls: ['./exercices-detail.component.scss']
})
export class ExercicesDetailComponent implements OnInit {

  @Input() exercise? : ExerciseDefinition;

  constructor() { }

  ngOnInit(): void {
  }

}

