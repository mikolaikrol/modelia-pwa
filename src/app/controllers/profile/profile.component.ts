import {Component, OnInit} from '@angular/core';
import {UserService} from "../../services/user/user.service";
import User from "../../models/User";
import {Activity} from "../../models/Activity";
import {LocalStorageService} from "../../services/local-storage/local-storage.service";
import {MonitorComponent} from "../../components/monitor/monitor.component";
import {NetworkService} from "../../services/network/network.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  loading = true;
  user: User | null;
  models: Activity[];
  trainings: Activity[];

  constructor(
    private userService: UserService,
    private storage: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    if (this.userService.isGuest()) {
      this.initUser();
    }
    this.userService.fetch().subscribe(resp => {
      this.userService.setUser(resp as User);
      this.initUser();
    }, () => {
      this.initUser();
    });
  }

  private initUser() {
    this.user = this.userService.getUser();
    if (this.userService.isGuest()) {
      const activities = this.storage.get('activities') ?? [];
      this.models = activities.filter(a => a.type === 'model') ?? [];
      this.trainings = activities.filter(a => a.type === 'training') ?? [];
    } else {
      this.models = this.user?.activities.filter(a => a.type === 'model') ?? [];
      this.trainings = this.user?.activities.filter(a => a.type === 'training') ?? [];
    }
    this.loading = false;
  }

}
